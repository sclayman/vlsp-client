import time
import json
from vimclient import VimClient

### testing

def pj(str):
    print(json.dumps(str))

vc = VimClient()
val1 = vc.create_router()
router1 = val1["routerID"]
pj(val1)

val2 = vc.create_router()
router2 = val2["routerID"]

pj(val2)

linkVal1 = vc.create_link(router1, router2)

pj(vc.list_routers())

pj(vc.list_links())

pj(vc.list_routers("all"))

time.sleep(20)

pj(vc.list_apps(router1))
pj(vc.list_apps(router2))

pj(vc.create_app(router2, "usr.applications.Recv", "4000"))
pj(vc.create_app(router1, "usr.applications.Send", str(router2) + " 4000 10000 -i 1 -b 20"))

time.sleep(60)   


pj(vc.delete_link(linkVal1["linkID"]))
   
pj(vc.delete_router(router2))

pj(vc.list_routers())

pj(vc.delete_router(router1))
