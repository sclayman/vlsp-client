# VLSP Vim Client
# s.clayman@ucl.ac.uk 
# tested on Python 2 and Python 3

import requests
import json

class VimClient:
    
    def _as_uri(host, port):
        return "http://" + host + ":" + str(port)

    def __init__(self, url=_as_uri("localhost", 8888)):
        self.url = url
        print(self.url)

    def vim_uri(self):
        return self.url

    def set_vim_address(self, host, port):
        self.url = _as_uri(host, port)

# ROUTER

#    Equivalent of: curl -X POST http://localhost:8888/router/
#    Returns JSONObject: {"address":"1","mgmtPort":11000,"name":"Router-1","r2rPort":11001,"routerID":1}
    def create_router(self):
        url = self.url + "/router/"
        result = requests.post(url)
        return result.json()

    def create_router_with_name(self, name):
        url = self.url + "/router/"
        result = requests.post(url, params={'name': name})
        return result.json()

    def create_router_with_address(self, address):
        url = self.url + "/router/"
        result = requests.post(url, params={'address': address})
        return result.json()

    def delete_router(self, id):
        url = self.url + "/router/" + str(id)
        result = requests.delete(url)
        return result.json()


#    Equivalent of: curl GET http://localhost:8888/router/
#    Returns JSONObject:  {"list":[12,6,5,7,8,9,10,1,3,11,4],"type":"router"}
    def list_routers(self, detail="id"):
        url = self.url + "/router/"
        result = requests.get(url, params={'detail': detail})
        return result.json()

    def list_removed_routers(self):
        url = self.url + "/removed/"
        result = requests.get(url)
        return result.json()

    def get_router_info(self, id):
        url = self.url + "/router/" + str(id)
        result = requests.get(url)
        return result.json()

    def get_router_link_stats(self, id):
        url = self.url + "/router/" + str(id) + "/link_stats"
        result = requests.get(url)
        return result.json()

    def get_router_link_stats(self, id, dstID):
        url = self.url + "/router/" + str(id) + "/link_stats/" + dstID
        result = requests.get(url)
        return result.json()

    def get_router_count(self, id):
        url = self.url + "/router/count" 
        result = requests.get(url)
        return result.json()

    def get_max_router_id(self, id):
        url = self.url + "/router/maxid" 
        result = requests.get(url)
        return result.json()

# LINK
    def create_link(self, routerID1, routerID2, weight=1):
        if isinstance(routerID1, int) and isinstance(routerID2, int):
            url = self.url + "/link/"
            result = requests.post(url, params={'router1': routerID1, 'router2' : routerID2, 'weight' : weight})
            return result.json()
        else:
            raise TypeError("create_link only accepts router IDs (as int)")

    def delete_link(self, id):
        url = self.url + "/link/" + str(id)
        result = requests.delete(url)
        return result.json()

    def list_links(self, detail="id"):
        url = self.url + "/link/"
        result = requests.get(url, params={'detail': detail})
        return result.json()

    def set_link_weight(self, weight=1):
        url = self.url + "/link/"
        result = requests.get(url, params={'weight': weight})
        return result.json()

    def get_link_info(self, id):
        url = self.url + "/link/" + str(id)
        result = requests.get(url)
        return result.json()

    def get_link_count(self, id):
        url = self.url + "/link/count" 
        result = requests.get(url)
        return result.json()

    def link_router_links(self, id):
        url = self.url + "/router/" + str(id) + "/link/" 
        result = requests.get(url)
        return result.json()

    def get_router_link_info(self, id, linkID):
        url = self.url + "/router/" + str(id) + "/link/" + linkID
        result = requests.get(url)
        return result.json()

# APP

    def create_app(self, id, className, args):
        url = self.url + "/router/" + str(id) + "/app/" 
        result = requests.post(url, params={'className': className, 'args' : args })
        return result.json()

    def stop_app(self, id, appID):
        url = self.url + "/router/" + str(id) + "/app/"  + str(appID)
        result = requests.delete(url)
        return result.json()

    def list_apps(self, id):
        url = self.url + "/router/" + str(id) + "/app/" 
        result = requests.get(url)
        return result.json()

    def get_app_info(self, id, appID):
        url = self.url + "/router/" + str(id) + "/app/" + str(appID)
        result = requests.get(url)
        return result.json()

# LOCALCONTROLLER

    def list_localcontrollers(self, detail="name"):
        url = self.url + "/localcontroller/"
        result = requests.get(url, params={'detail': detail})
        return result.json()

    def get_localcontroller_info(self, arg):
        url = self.url + "/localcontroller/"
        result = requests.get(url, params={'name': arg})
        return result.json()

    def set_localcontroller_online(self, arg):
        url = self.url + "/localcontroller/" + str(arg)
        result = requests.put(url, params={'status': 'online'})
        return result.json()

    def set_localcontroller_offline(self, arg):
        url = self.url + "/localcontroller/" + str(arg)
        result = requests.put(url, params={'status': 'offline'})
        return result.json()

    
# GRAPH

    def network_visualization(self, style="dot"):
        url = self.url + "/graph/" + style
        result = requests.get(url)
        return result.json()

