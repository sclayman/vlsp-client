import time
import json
from vimclient import VimClient

# Create a topology of 3 linked routers
def topology123(vlsp):
    r1 = vlsp.create_router()
    r2 = vlsp.create_router()
    r3 = vlsp.create_router()
    #
    l1 = vlsp.create_link(r1["routerID"], r2["routerID"])
    l2 = vlsp.create_link(r2["routerID"], r3["routerID"])
    #
    return {'routers': [r1, r2, r3], 'links': [l1, l2]}


# Delete all the routers
def delete_all(vlsp):
    allRouters = vlsp.list_routers()["list"]
    #
    map(lambda i: vlsp.delete_router(i), allRouters)
    return None

# Get the FN node from a topology
# It's the 2nd one of 3 !
def fn_node(vlsp):
    routers = vlsp.list_routers()
    #
    if not routers:
        return None
    elif len(routers["list"]) == 3:
        return routers["list"][1]
    else:
        return None


# Get the apps on the FN node of a topology
def fn_node_apps(vlsp):
    fn_node_no = fn_node(vlsp)
    #
    if fn_node_no is None:
        return None
    else:
        return vlsp.list_apps(fn_node_no)


# Get the apps on the FN node of a topology
# given a router ID
def fn_node_apps(vlsp, fn_node_no):
    return vlsp.list_apps(fn_node_no)


# Get all the app info  on the FN node of a topology
def fn_node_app_info(vlsp):
    fn_node_no = fn_node(vlsp)
    #
    if fn_node_no is None:
        return None
    else:
        node_apps = fn_node_apps(vlsp, fn_node_no)
        if len(node_apps["list"]) > 0:
            return vlsp.get_app_info(fn_node_no, node_apps["list"][0])
        else:
            return None


# Start an app on the FN node of a topology
def start_fn(vlsp, className, args):
    fn_node_no = fn_node(vlsp)
    #
    if fn_node_no is None:
        return None
    else:
        # NetFn intercepts packets
        return vlsp.create_app(fn_node_no, className, args)


# Stop an app on the FN node of a topology
def stop_fn(vlsp):
    fn_node_no = fn_node(vlsp)
    #
    if fn_node_no is None:
        return None
    else:
        node_apps = fn_node_apps(vlsp, fn_node_no)
        if len(node_apps["list"]) > 0:
            return vlsp.stop_app(fn_node_no, node_apps["list"][0])
        else:
            return None


# Start the BPP Fn
def bpp_node(vlsp, bandwidth):
    return start_fn(vlsp, "demo_usr.bpp.BPPFn", "-b " + str(bandwidth) + " -r 160")


        
# Start the UDP Fn
def udp_node(vlsp, bandwidth):
    return start_fn(vlsp, "demo_usr.bpp.UDPFn", "-b " + str(bandwidth) + " -r 160")


# Start the Ingress and Egress nodes
# Default ingress port: 6799
# Default egress port: 6798
def ingress_and_egress(vlsp, ingress_port=6799, egress_port=6798):
    routers = vlsp.list_routers()
    #
    if not routers:
        return None
    elif len(routers["list"]) == 3:
        ingress_node = routers["list"][0]
        egress_node = routers["list"][2]
        a1 = vlsp.create_app(ingress_node, "demo_usr.paths.Ingress", str(ingress_port) + " " + str(egress_node) + ":4567")
        a3 = vlsp.create_app(egress_node, "demo_usr.paths.Egress", "4567  localhost:" + str(egress_port))
        # return app data
        return [a1, a3]
    else:
        return None


# print json nicely
def pj(str):
    print(json.dumps(str))
    

    


