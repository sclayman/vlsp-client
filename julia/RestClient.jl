struct RestClient
    uri::AbstractString
    port::Int

    RestClient() = RestClient("localhost", 8888)
    RestClient(host::AbstractString, p::Int) =
        let vimURI = "http://" * host * ":" * string(p)
            new(vimURI, p)
        end
end

