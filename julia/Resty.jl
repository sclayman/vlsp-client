module Resty


using HTTPClient.HTTPC
using JSON

export json, get, post, put, delete
export urlencode

# wrapper types
immutable Post{V}
       d::Dict{AbstractString, V}
end

immutable Put{V}
       d::Dict{AbstractString, V}
end

immutable Delete
end

# functions to wrap Dicts
# and create wrapper types
function post(d::Dict)
    Post(d)
end

function put(d::Dict)
    Put(d)
end
function delete()
    Delete()
end

# GET
# json("http://localhost:8888/router/")
function get(url::AbstractString)
    JSON.parse(bytestring(HTTPC.get(url).body))
end

function json(url::AbstractString)
    JSON.parse(bytestring(HTTPC.get(url).body))
end

# POST
# r=HTTPC.post(RB * "?test=6.1.0", @compat Dict("a" => 1, "b" => 2))
function post(url::AbstractString, d::Dict{AbstractString,Any})
    JSON.parse(bytestring(HTTPC.post(url, d).body))
end

function json(url::AbstractString, d::Dict{AbstractString,Any})
    JSON.parse(bytestring(HTTPC.post(url, d).body))
end

function json(url::AbstractString, p::Post{Any})
    JSON.parse(bytestring(HTTPC.post(url, p.d).body))
end

# PUT
function put(url::AbstractString, d::Dict{AbstractString,Any})
    JSON.parse(bytestring(HTTPC.put(url, d).body))
end

function json(url::AbstractString, p::Put{Any})
    JSON.parse(bytestring(HTTPC.put(url, p.d).body))
end

# DELETE
function delete(url::AbstractString)
    JSON.parse(bytestring(HTTPC.delete(url).body))
end

function json(url::AbstractString, d::Delete)
    JSON.parse(bytestring(HTTPC.delete(url).body))
end

function urlencode(s::AbstractString)
    HTTPC.urlencode(s)
end
    

end
