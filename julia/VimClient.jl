module VimClient

include("./Resty.jl")
#import Resty
#using Resty


import Base.show

export RestClient, show
export createRouter, createRouterWithName, createRouterWithAddress, deleteRouter, listRouters, listRemovedRouters, getRouterInfo, getRouterLinkStats, getRouterCount, getMaxRouterID
export createLink, deleteLink, listLinks, getLinkInfo, setLinkWeight, getLinkCount, listRouterLinks, getRouterLinkInfo
export createApp, stopApp, listApps, getAppInfo
export listLocalControllers, getLocalControllerInfo, setLocalControllerStatus


include("./RestClient.jl")


function Base.show(io::IO, v::RestClient)
    print(io, v.uri)
end

# ROUTER

#=
    Equivalent of: curl -X POST http://localhost:8888/router/
    Returns JSONObject: {"address":"1","mgmtPort":11000,"name":"Router-1","r2rPort":11001,"routerID":1}
=#
function createRouter(v::RestClient)
    let
        url = v.uri * "/router/"
        Resty.post(url, Dict{AbstractString,Any}())
    end
end

function createRouter(v::RestClient, name::AbstractString, address::AbstractString)
    let
        url = v.uri * "/router/?name=" * name * "&address=" * address
        Resty.post(url, Dict{AbstractString,Any}())
    end
end

function createRouterWithName(v::RestClient, name::AbstractString)
    let
        url = v.uri * "/router/?name=" * name 
        Resty.post(url, Dict{AbstractString,Any}())
    end
end

function createRouterWithAddress(v::RestClient, address::AbstractString)
    let
        url = v.uri * "/router/?address=" * address
        Resty.post(url, Dict{AbstractString,Any}())
    end
end

function deleteRouter(v::RestClient, id::Int)
    let
        url = v.uri * "/router/" * string(id)
        Resty.delete(url)
    end
end

#=
    Equivalent of: curl GET http://localhost:8888/router/
    Returns JSONObject:  {"list":[12,6,5,7,8,9,10,1,3,11,4],"type":"router"}
=#
function listRouters(v::RestClient)
    let
        url = v.uri * "/router/"
        Resty.get(url)
    end
end

function listRemovedRouters(v::RestClient)
    let
        url = v.uri * "/removed/"
        Resty.get(url)
    end
end

function getRouterInfo(v::RestClient, id::Int)
    let
        url = v.uri * "/router/" * string(id)
        Resty.get(url)
    end
end

function getRouterLinkStats(v::RestClient, id::Int)
    let
        url = v.uri * "/router/" * string(id) * "/link_stats"
        Resty.get(url)
    end
end

function getRouterLinkStats(v::RestClient, id::Int, dstID::Int)
    let
        url = v.uri * "/router/" * string(id) * "/link_stats/" * string(dstID)
        Resty.get(url)
    end
end

function getRouterCount(v::RestClient)
    let
        url = v.uri * "/router/count"
        Resty.get(url)
    end
end

function getMaxRouterID(v::RestClient)
    let
        url = v.uri * "/router/maxid"
        Resty.get(url)
    end
end

# LINK

function createLink(v::RestClient, routerID1::Int, routerID2::Int, weight::Int = 1)
    let
        url = v.uri * "/link/?router1=" * string(routerID1) * "&router2=" * string(routerID2)  * "&weight=" * string(weight);
        Resty.post(url, Dict{AbstractString,Any}())
    end
end

function createLink(v::RestClient, routerID1::Int, routerID2::Int, weight::Int, linkName::AbstractString)
    let
        url = v.uri * "/link/?router1=" * string(routerID1) * "&router2=" * string(routerID2)  * "&weight=" * string(weight) * "&linkName=" * linkName
        Resty.post(url, Dict{AbstractString,Any}())
    end
end


# createLink - this version take 2 Dicts
function createLink(v::RestClient, router1::Dict{String, Any}, router2::Dict{String, Any}, weight::Int = 1)
    let
        routerID1 = router1["routerID"]
        routerID2 = router2["routerID"]
        url = v.uri * "/link/?router1=" * string(routerID1) * "&router2=" * string(routerID2)  * "&weight=" * string(weight);
        Resty.post(url, Dict{AbstractString,Any}())
    end
end


function deleteLink(v::RestClient, id::Int)
    let
        url = v.uri * "/link/" * string(id)
        Resty.delete(url)
    end
end

function listLinks(v::RestClient)
    let
        url = v.uri * "/link/"
        Resty.get(url)
    end
end

function listLinks(v::RestClient, detail::AbstractString)
    let
        url = v.uri * "/link/?detail=" * detail
        Resty.get(url)
    end
end

function getLinkInfo(v::RestClient, id::Int)
    let
        url = v.uri * "/link/" * string(id)
        Resty.get(url)
    end
end

function setLinkWeight(v::RestClient, linkID::Int, weight::Int = 1)
    let
        url = v.uri * "/link/" * string(linkID) * "?weight=" * string(weight);
        Resty.put(url, Dict{AbstractString,Any}())
    end
end

function getLinkCount(v::RestClient)
    let
        url = v.uri * "/link/count" 
        Resty.get(url)
    end
end

function listRouterLinks(v::RestClient, id::Int)
    let
        url = v.uri * "/router/" * string(id) * "/link/"
        Resty.get(url)
    end
end

function getRouterLinkInfo(v::RestClient, id::Int, linkID::Int)
    let
        url = v.uri * "/router/" * string(id) * "/link/" * string(linkID)
        Resty.get(url)
    end
end

# APP

function createApp(v::RestClient, routerID::Int, className::AbstractString, args::AbstractString)
    let
        url = v.uri * "/router/" * string(routerID) * "/app/?className=" * className * "&args=" * Resty.urlencode(args) # 
        Resty.post(url, Dict{AbstractString,Any}())
    end
end

function stopApp(v::RestClient, routerID::Int, appID::Int)
    let
        url = v.uri * "/router/" * string(routerID) * "/app/" * string(appID)
        Resty.delete(url)
    end
end

function listApps(v::RestClient, id::Int)
    let
        url = v.uri * "/router/" * string(id) * "/app/"
        Resty.get(url)
    end
end

function getAppInfo(v::RestClient, id::Int, appID::Int)
    let
        url = v.uri * "/router/" * string(id) * "/app/" * string(appID)
        Resty.get(url)
    end
end

# LOCALCONTROLLER

function listLocalControllers(v::RestClient)
    let
        url = v.uri * "/localcontroller/"
        Resty.get(url)
    end
end

function getLocalControllerInfo(v::RestClient, name::AbstractString)
    let
        url = v.uri * "/localcontroller/" * name
        Resty.get(url)
    end
end

function setLocalControllerStatus(v::RestClient, name::AbstractString, status::AbstractString)
    let
        url = v.uri * "/localcontroller/" * name * "?status=" * status
        Resty.put(url, Dict{AbstractString,Any}())
        Resty.get(url)
    end
end



end
