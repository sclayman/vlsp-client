#include("./RestClient.jl")


# Create a topology of 3 linked routers
function topology123(vlsp::VimClient.RestClient)
    let r1 = VimClient.createRouter(vlsp)
        r2 = VimClient.createRouter(vlsp)
        r3 = VimClient.createRouter(vlsp)

        #println([r1, r2, r3])

        let l1 = VimClient.createLink(vlsp, r1, r2)
            l2 = VimClient.createLink(vlsp, r2, r3)

            # println([l1, l2])

            Dict("routers" => [r1, r2, r3], "links" => [l1, l2])
        end
    end
end


                           

# Delete all the routers
function deleteAll(vlsp::VimClient.RestClient)
    let allRouters = VimClient.listRouters(vlsp)["list"]
        map(i -> VimClient.deleteRouter(vlsp, i), allRouters)
    end
end



# Get the FN node from a topology
# It's the 2nd one of 3 !
function fn_node(vlsp::VimClient.RestClient) ::Union{Missing, Int}
    let routers = VimClient.listRouters(vlsp)

        if isempty(routers)
            missing
        elseif length(routers["list"]) == 3
            routers["list"][2]
        else missing
        end
    end
end

# Get the apps on the FN node of a topology
function fn_node_apps(vlsp::VimClient.RestClient) 
    let fn_node = fn_node(vlsp)
        if ismissing(fn_node)
            missing
        else 
            VimClient.listApps(vlsp, fn_node)
        end
    end
end
    
# Get the apps on the FN node of a topology
# given a router ID
function fn_node_apps(fn_node::Int) 
    let
        # TODO: pass in vlsp
        VimClient.listApps(vlsp, fn_node)
    end
end
    
# Get all the app info  on the FN node of a topology
function fn_node_app_info(vlsp::VimClient.RestClient)
    let fn_node = fn_node(vlsp)
        if ismissing(fn_node)
            missing
        else
            let node_apps = fn_node_apps(fn_node)
                # TODO: check if fn_node_apps has a length
                VimClient.getAppInfo(vlsp, fn_node, fn_node_apps(fn_node)["list"][1])
            end
        end
    end
end

# Start an app on the FN node of a topology
function start_fn(vlsp::VimClient.RestClient, className::AbstractString, args::AbstractString) 
    let fn_node = fn_node(vlsp)
        if ismissing(fn_node)
            missing
        else
            # NetFn intercepts packets
            VimClient.createApp(vlsp, fn_node, className, args)
        end
    end
end

# Stop an app on the FN node of a topology
function stop_fn(vlsp::VimClient.RestClient)
    let fn_node = fn_node(vlsp)
        if ismissing(fn_node)
            missing
        else
            let node_apps = fn_node_apps(fn_node)["list"][1]
                VimClient.stopApp(vlsp, fn_node, node_apps[1])
            end
        end
    end
end



# Start the BPP Fn
function bpp_node(vlsp::VimClient.RestClient, bandwidth::Number)
    start_fn(vlsp, "demo_usr.bpp.BPPFn", "-b " * string(bandwidth) * " -r 160")
end


# Start the UDP Fn
function udp_node(vlsp::VimClient.RestClient, bandwidth::Number)
    start_fn(vlsp,  "demo_usr.bpp.UDPFn", "-b " * string(bandwidth) * " -r 160")
end


# Start the Ingress and Egress nodes
function ingress_and_egress(vlsp::VimClient.RestClient)
    let routers = VimClient.listRouters(vlsp)

        if isempty(routers)
            missing
        elseif length(routers["list"]) == 3
            let ingress_node = routers["list"][1]
                egress_node = routers["list"][3]
                a1 = VimClient.createApp(vlsp, ingress_node, "demo_usr.paths.Ingress", "6799 " * string(egress_node) * ":4567")
                a3 = VimClient.createApp(vlsp, egress_node, "demo_usr.paths.Egress", "4567  localhost:6798")
                # return app data
                [a1, a3]
            end
        else missing
        end
    end
end


