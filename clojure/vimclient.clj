(import 'us.monoid.json.JSONObject 'us.monoid.json.JSONArray 'us.monoid.web.Resty)

(require 'resty)

;;; SUPPORT FUNCTIONS

;; is a value in a seq
(defn in? 
  "true if a seq contains elem"
  [aseq elem]  
  (some #(= elem %) aseq))


;;; convert an iterator to a vector
(defn iterator-vector 
  "Convert an iterator to a vector"
  [iter]
  (loop [coll (vector)] 
    (if (.hasNext iter) 
      (recur (conj coll (.next iter))) 
      coll))) 

;; Read a value from a file
(defn read-object
  "Read a value from a file"
  [file-path]
  (with-open [rdr (clojure.java.io/reader file-path)]
    (let [val (line-seq rdr)]
      (read-string (first val)))))


;; Write a value to a file
(defn write-object
  "Write a value to a file"
  [file-path obj]
  (with-open [wtr (clojure.java.io/writer file-path)]
      (.write wtr (pr-str obj))))


;; catch a stack trace
(defmacro catch-stack-trace
  "Evaluate a form and print a stack trace if the is an Exception"
  [form]
  `(try 
     ~form 
        (catch java.lang.Exception e# (. e# printStackTrace))))

;; catch a stack trace and show message
(defmacro catch-msg
  "Evaluate a form and print an error message if the is an Exception"
  [form]
  `(try 
     ~form 
        (catch java.lang.Exception e# (. e# getMessage))))



;;; ** Set up **

(defn- as-uri
  [host port]
  (str "http://" host ":" port))

(def ^:dynamic
  ^{:doc "The URI of the VIM Management Console"
    }
  *vim-uri* (as-uri "localhost" 8888)) ;;; (atom "http://localhost:8888")


(defn- initialize
  "Initialize VIM URI given host and port"
  [host port]
  (alter-var-root (var *vim-uri*) (constantly (as-uri host port))))
  ;;;(reset! *vim-uri* (str "http://" host ":" port)))


(defn vim-uri
  "Get the VIM URI"
  []  *vim-uri*)

(defn set-vim-address
  "Reset the VIM URI given a host and port"
  [host port]
  (initialize host port))



(defmacro with-vim-address
  "Evaluate the body on a different VIM specifying ther host and port"
  [host port & body]
  `(binding [*vim-uri* (as-uri ~host ~port)] ; rebind the *vim-uri* temporarily
    ~@body))

;;;  ** Routers **

(defn create-router
  "Create a router.
Can specify name and address if needed"
  ([] (let [rest (Resty. nil) 
            uri (str *vim-uri* "/router/")
            result (. rest (json uri (Resty/form "")))]
        (from-resty (.toObject result) )) )
  ([name address] (let [rest (Resty. nil) 
                        uri (str *vim-uri* "/router/?name=" name "&address=" address)
                        result (. rest (json uri (Resty/form "")))]
                    (from-resty (.toObject result) ))   )
  )

(defn create-router-with-name
  "Create a router with a specified name."
  ([] {:error "Need a router name"})
  ([name] (let [rest (Resty. nil) 
                uri (str *vim-uri*  "/router/?name=" name)
                result (. rest (json uri (Resty/form "")))]
            (from-resty (.toObject result) ))   )
  )

(defn create-router-with-address
  "Create a router with a specified address."
  ([] {:error "Need a router address"})
  ([address] (let [rest (Resty. nil) 
                   uri (str *vim-uri*  "/router/?address=" address)
                   result (. rest (json uri (Resty/form "")))]
               (from-resty (.toObject result) ))   )
  )


(defn delete-router
  "Delete a router, specified by router ID"
  ([] {:error "Need a router ID"})
  ([routerID] (let [rest (Resty. nil) 
                   uri (str *vim-uri* "/router/" routerID)
                   result (. rest (json uri (Resty/delete)))]
               (from-resty (.toObject result) ) ) ) )


(defn list-routers
  "List all routers.
Can optionally specify details as one of: all,  id"
  ([] (let 
          [rest (Resty. nil) 
           uri (str *vim-uri* "/router/")
           result (. rest (json uri))]
        (from-resty (.toObject result) ) ) )
  ([detail] 
     (if (in? ["all" "id"] detail)
       (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/router/?detail=" detail)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) )
       nil
       ) )
  )

(defn delete-all-routers
  "Delete all the routers"
  []  (map delete-router ((list-routers) :list)))

;;(defn delete-all-routers  [] (map #(delete-router %) (:list (list-routers))))

(defn list-removed-routers
  "List all routers.
Can optionally specify details as one of: all,  id"
  ([] (let 
          [rest (Resty. nil) 
           uri (str *vim-uri* "/removed/")
           result (. rest (json uri))]
        (from-resty (.toObject result) ) ) )
)

(defn router-info
  "Get some info on a router"
  ([] {:error "Need a router ID"})
  ([id] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/router/" id)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) ) )

(defn router-count
  "Get the count of the no of routers"
  [] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/router/" "count")
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) )

(defn max-router-id
  "Get the maximum ID of a router"
  [] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/router/" "maxid")
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) )

(defn router-link-stats
  "Get link stats data on a router"
  ([] {:error "Need a router ID"})
  ([id] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/router/" id "/link_stats")
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) )
  ([id dst] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/router/" id "/link_stats" "/" dst)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) ) )


;;;  ** Links **

(defn create-link
  "Create a link
Optionally specify link weight and link name"
  ([] {:error "Need a source router ID and a destination router ID"})
  ([arg] {:error "Need a source router ID and a destination router ID"})
  ([routerID1 routerID2]
     (let [rest (Resty. nil) 
           uri (str *vim-uri* "/link/?router1=" routerID1 "&router2=" routerID2 "&weight=1")
           result (. rest (json uri (Resty/form "")))]
       (from-resty (.toObject result) )))
  ([routerID1 routerID2 weight]
     (let [rest (Resty. nil) 
           uri (str *vim-uri* "/link/?router1=" routerID1 "&router2="  routerID2 "&weight=" weight)
           result (. rest (json uri (Resty/form "")))] 
       (from-resty (.toObject result) )) )
  ([routerID1 routerID2 weight linkName]
     (let [rest (Resty. nil) 
           uri (str *vim-uri* "/link/?router1=" routerID1 "&router2=" routerID2 "&weight="  weight "&linkName=" linkName)
           result (. rest (json uri (Resty/form "")))] 
       (from-resty (.toObject result) )) )
  )



(defn delete-link
  "Delete a link, specified by link ID"
  ([] {:error "Need a link ID"})
  ([linkID] (let [rest (Resty. nil) 
                 uri (str *vim-uri* "/link/" linkID)
                 result (. rest (json uri (Resty/delete)))]
             (from-resty (.toObject result) ) ) ) )

(defn list-links
  "List all links
Can optionally specify details as one of: all,  id"
  ([] (let 
          [rest (Resty. nil) 
           uri (str *vim-uri* "/link/")
           result (. rest (json uri))]
        (from-resty (.toObject result) ) ))
  ([detail] 
     (if (in? ["all" "id"] detail)
       (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/link/?detail=" detail)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) )
       nil
       ) )
  )

(defn delete-all-links
  "Delete all the links"
  []  (map delete-link (:list (list-links))))

;;(defn delete-all-routers  [] (map #(delete-router %) (:list (list-routers))))


(defn link-info
  "Get some info on a link"
  ([] {:error "Need a link ID"})
  ([id] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/link/" id)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) ) )


(defn set-link-weight
  "Set the weight of a link"
  ([] {:error "Need a link ID and a weight"})
  ([arg] {:error "Need a link ID and a weight"})
  ([linkID weight]
     (let [rest (Resty. nil) 
           uri (str *vim-uri* "/link/" linkID  "?weight=" weight)
           result (. rest (json uri (Resty/put (Resty/content ""))))]
       (from-resty (.toObject result) )) ) )


(defn link-count
  "Get the count of the no of links"
  [] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/link/" "count")
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) )


(defn list-router-links
  "Get the links of a router"
  ([] {:error "Need a router ID"})
  ([id] (let 
            [rest (Resty. nil) 
             uri (str *vim-uri* "/router/" id "/link/")
             result (. rest (json uri))]
          (from-resty (.toObject result) ) ) )
  ([id attr]
     (if (in? ["id" "name" "weight" "connected"] attr)
       (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/router/" id "/link/" "?attr=" attr)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) ))
  )


(defn router-link-info
  "Get some info in a link of a router, specify routerID and linkID"
  ([] {:error "Need a router ID and a link ID"})
  ([arg] {:error "Need a router ID and a link ID"})
  ([routerID linkID] (let 
                        [rest (Resty. nil) 
                         uri (str *vim-uri* "/router/" routerID "/link/" linkID)
                         result (. rest (json uri))]
                      (from-resty (.toObject result) ) ) ) )

(defn router-link-info
  ([] {:error "Need a router ID"})
  ([routerID]
      (let [r (router-link-stats routerID)] (zipmap (:links r) (:link_stats r)))) )


;;;  ** Apps **


(defn create-app
  "Create an app.
Args are: routerID className appArgs"
  ([] {:error "Need a router ID, class name, and some args"})
  ([routerID class-name args]
     (let [rest (Resty. nil) 
           encoded-args (java.net.URLEncoder/encode args "UTF-8")
           uri (str *vim-uri* "/router/" routerID "/app/?className=" class-name "&args=" encoded-args)
           result (. rest (json uri (Resty/form "")))]
       (from-resty (.toObject result) )) ) )


(defn stop-app
  "Delete a router, specified by router ID"
  ([] {:error "Need a router ID and appID"})
  ([arg] {:error "Need a router ID and appID"})
  ([routerID appID] (let [rest (Resty. nil) 
                          uri (str *vim-uri* "/router/" routerID "/app/" appID)
                          result (. rest (json uri (Resty/delete)))]
                      (from-resty (.toObject result) ) ) ) )



(defn list-apps
  "List all apps on a router"
  ([] {:error "Need a router ID"})
  ([routerID] (let 
                  [rest (Resty. nil) 
                   uri (str *vim-uri* "/router/" routerID "/app/")
                   result (. rest (json uri))]
                (from-resty (.toObject result) ) ) ) )


(defn app-info
  "Get info on an app on a router, specify routerID and appID"
  ([] {:error "Need a router ID and appID"})
  ([arg] {:error "Need a router ID and appID"})
  ([routerID appID] (let 
                        [rest (Resty. nil) 
                         uri (str *vim-uri* "/router/" routerID "/app/" appID)
                         result (. rest (json uri))]
                      (from-resty (.toObject result) ) ) ) )


;;; ** Localcontroller **
(defn list-localcontrollers
  "List all Local Controllers.
Can optionally specify details as one of: all,  id"
  ([] (let 
          [rest (Resty. nil) 
           uri (str *vim-uri* "/localcontroller/")
           result (. rest (json uri))]
        (from-resty (.toObject result) ) ) )
  ([detail] 
     (if (in? ["all"] detail)
       (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/localcontroller/?detail=" detail)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) )
       nil
       ) )
  )

(defn localcontroller-info
  "Get some info on a localcontroller"
  ([] {:error "Need a localcontroller name or address"})
  ([arg] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/localcontroller/?name=" arg)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) ) )


(defn localcontroller-online
  "Make a specified localcontroller ONLINE"
  ([] {:error "Need a localcontroller name or address"})
  ([arg]
     (let [rest (Resty. nil) 
           uri (str *vim-uri* "/localcontroller/" arg "?status=online")
           result (. rest (json uri (Resty/put (Resty/content ""))))]
       (from-resty (.toObject result) ) ) ) )


(defn localcontroller-offline
  "Make a specified localcontroller OFFLINE"
  ([] {:error "Need a localcontroller name or address"})
  ([arg]
     (let [rest (Resty. nil) 
           uri (str *vim-uri* "/localcontroller/" arg "?status=offline")
           result (. rest (json uri (Resty/put (Resty/content ""))))]
       (from-resty (.toObject result) ) ) ) )



;;;  ** Agg Points **

(defn list-agg-points
  "List all aggregation points"
  ([] (let 
                  [rest (Resty. nil) 
                   uri (str *vim-uri* "/ap/")
                   result (. rest (json uri))]
                (from-resty (.toObject result) ) ) ) )


(defn agg-point-info
  "Get some info on an aggregation point"
  ([] {:error "Need an agg point ID"})
  ([id] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/ap/" id)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) ) )


(defn set-agg-point
  "Set a router to point to an aggregation point
specify routerID and apID"
  ([] {:error "Need a router ID and an agg point ID"})
  ([arg] {:error "Need a router ID and an agg point ID"})
  ([routerID apID] (let [rest (Resty. nil) 
                uri (str *vim-uri*  "/ap/?apID=" apID "&routerID=" routerID)
                result (. rest (json uri (Resty/form "")))]
            (from-resty (.toObject result) ))   )
  )


(defn network-visualization
  "Get a visualize of the network graph"
  ([] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/graph/" "dot")
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) )
  ([class] (let 
           [rest (Resty. nil) 
            uri (str *vim-uri* "/graph/" class)
            result (. rest (json uri))]
         (from-resty (.toObject result) ) ) ) )

;;; DERIVED FUNCTIONS
;;; These are useful additions to the base VIM functions

;; list online localcontrollers
(defn list-online-localcontrollers
  []
  (let [the-list (list-localcontrollers)
        type (:type the-list)
        localcontrollers-list (:list the-list)]
        
    {:type type :list (vec (filter #(= "ONLINE" (:status (first (:detail (localcontroller-info %))))) localcontrollers-list)) } ))

;; list offline localcontrollers
(defn list-offline-localcontrollers
  []
  (let [the-list (list-localcontrollers)
        type (:type the-list)
        localcontrollers-list (:list the-list)]
        
    {:type type :list (vec (filter #(= "OFFLINE" (:status (first (:detail (localcontroller-info %))))) localcontrollers-list)) } ))

;; calculate the maximum number of routers all the online
;; localcontrollers can support
(defn max-routers-localcontrollers
  "Calculate the maximum number of routers all the online localcontrollers can place"
  []
  (reduce + (map #(:maxRouters (first (:detail (localcontroller-info %)))) (:list (list-online-localcontrollers)))))

;; do some function after a certain no of seconds
(defmacro after
  "Do some function after a certain no of seconds"
  [n form]
  (if (= n 0) form                      ; do fn immediately if seconds == 0
      `(do (Thread/sleep (* ~n 1000)) ~form))) ;sleep then do function




