(import 'us.monoid.json.JSONObject 'us.monoid.json.JSONArray 'us.monoid.web.Resty)


;;; ** convert from Resty into clojure forms **
(defmulti from-resty class)

;;; convert JSONObject into clojure forms
(defmethod from-resty JSONObject [jsobj] 
  (let [keys (iterator-seq (. jsobj keys)) ; convert iterator to a seq 'iterator-seq'
        values (map #(from-resty (. jsobj (get %))) keys)] ; get values by accessing via keys
    (zipmap (map #(keyword %) keys) values)   ; zip up keys and values - cvt keys to keywords
    )
  )

;;; convert JSONArray into clojure forms
(defmethod from-resty JSONArray [jsarr] 
  (let [index (range 0 (. jsarr length))]
    (vec (map #(from-resty (. jsarr  (get %))) index))
    ) )

;;; convert other Resty forms into clojure forms
(defmethod from-resty :default [obj] obj)


;;; ** convert from  clojure forms into Resty forms **
(defmulti to-resty class)

;;; convert  List clojure forms into JSONArray
(defmethod to-resty java.util.List [arr] 
  (JSONArray. (java.util.ArrayList. (map to-resty arr)))
    )

;;; convert  Vector clojure forms into JSONArray
(defmethod to-resty clojure.lang.PersistentVector [arr] 
  (JSONArray. (java.util.ArrayList. (map to-resty arr)))
    )

;;; convert Map clojure forms into JSONObject
(defmethod to-resty clojure.lang.PersistentArrayMap [map] 
  (let [;keys (keys map) ; get keys
        ;values  (map to-resty  (vals m)) ; get values by accsing via keys
        entries (. map entrySet)
        jsobj (JSONObject.)]
    (do 
      (reduce (fn [init [k v]] (. jsobj (put (name k) (to-resty v)))) jsobj entries)
    jsobj) ) )

;;; convert Map clojure forms into JSONObject
(defmethod to-resty clojure.lang.PersistentHashMap [map] 
  (let [;keys (keys map) ; get keys
        ;values  (map to-resty  (vals m)) ; get values by accsing via keys
        entries (. map entrySet)
        jsobj (JSONObject.)]
    (do 
      (reduce (fn [init [k v]] (. jsobj (put (name k) (to-resty v)))) jsobj entries)
      jsobj) ) )

;;; convert other clojure forms into resty forms
(defmethod to-resty :default [obj] obj)


